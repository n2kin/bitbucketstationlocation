import { Component } from '@angular/core';

import { AuthService } from '../authService';

@Component({
  templateUrl: './login-component.html',
})

export class LoginComponent {

  constructor(private authService: AuthService) {}

  login(email, password) {
    if(!email.value || !password.value) {
      alert("enter email/ password")
      return;
  }
  this.authService.logInUser(email.value, password.value)
}
    
  
}