import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })

export class AuthService {

  private token: string;
  private id: string;
  private company: string;
  private authStatus = new Subject<boolean>();

  getToken() {
    return this.token
  }

    getId() {
    return this.id
  }

  getCompany() {
    return this.company;
  }

  getAuthStatus() {
    return this.authStatus.asObservable();
  }

  constructor(private http: HttpClient, private router: Router) {}

    createUser(company, email, password) {
    const authData = { company: company, email: email, password: password };
    this.http.post<{message}>('http://localhost:3000/api/user/signup', authData)
      .subscribe(responce => {
        alert(responce.message)
        
      })
      this.router.navigate([''])
    }

    logInUser(email, password) {
      const authData = { email: email, password: password };
      this.http.post<{token: string, id: string, message, company : string}>('http://localhost:3000/api/user/login', authData)
        .subscribe(responce => {
        const token = responce.token;
        const id = responce.id;
        const company = responce.company;
        this.token = token;
        this.id = id
        this.company = company;
        console.log(responce);
        alert(responce.message);
        this.authStatus.next(true);
        this.router.navigate(['/list'])
      })
    }

    logOutUser() {
      this.token = '';
      alert('User loged out');
      this.authStatus.next(false);
      this.router.navigate([''])
    }
    

}

