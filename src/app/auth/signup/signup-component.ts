import { Component } from '@angular/core';

import { AuthService } from '../authService';


@Component({
  templateUrl: './signup-component.html',
})

export class SignInComponent {

constructor(private authService: AuthService) {}

onSignup(company: HTMLInputElement, email: HTMLInputElement, password: HTMLInputElement) {
  console.log(company)
  if(!company.value || !email.value || !password.value) {
    alert("please fill all the fields")
    return;
  }
  this.authService.createUser(company.value, email.value, password.value)
}

}