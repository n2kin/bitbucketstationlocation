import { Component } from '@angular/core';
import { ListComponent } from '../list/list.component';
import { UserInput } from '../user/user.input';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent  {
  counter = 0;
  userInputListener() {
    this.counter ++
    console.log('event emiter works');
  }
}
