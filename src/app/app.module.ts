import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ListComponent } from '../list/list.component';
import { UserInput } from '../user/user.input';
import { LoginComponent } from './auth/login/login-component';
import { SignInComponent } from './auth/signup/signup-component';
import { HeaderClass } from '../header/header.component';
import { AppRoutungModule } from './app-routing.module';

import { HTTPService } from '../http/http.service';

@NgModule({
  declarations: [ 
    AppComponent, 
    ListComponent, 
    UserInput, 
    LoginComponent,
    SignInComponent,
    HeaderClass, 
  ],
  imports: [ BrowserModule, FormsModule, HttpClientModule, AppRoutungModule ],
  providers: [ HTTPService ],
  bootstrap:[ AppComponent ]
})
export class AppModule {}
