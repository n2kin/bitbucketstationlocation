import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListComponent } from '../list/list.component';
import { LoginComponent } from './auth/login/login-component';
import { SignInComponent } from './auth/signup/signup-component';
import { UserInput } from '../user/user.input';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'list', component: ListComponent },
  { path: 'signin', component: SignInComponent },
  { path: 'new', component: UserInput },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutungModule {}