import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginComponent } from '../app/auth/login/login-component';
import { SignInComponent } from '../app/auth/signup/signup-component';

import { AuthService } from "../app/auth/authService";

@Component({
  selector: 'header',
  templateUrl: 'header.component.html',
})

export class HeaderClass implements OnInit, OnDestroy {

  userIsAuthenticated = false;
  authStatusListener: Subscription;

  constructor(private auth: AuthService) {}

  ngOnInit() {
    this.authStatusListener = this.auth.getAuthStatus().subscribe(isAuthenticated => { this.userIsAuthenticated = isAuthenticated });
  }

  ngOnDestroy() {
    this.authStatusListener.unsubscribe()
  }
}
