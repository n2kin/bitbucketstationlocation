import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { HTTPService } from '../http/http.service';
import { AuthService } from "../app/auth/authService";

@Component({
  selector: 'list', 
  templateUrl: './list.component.html',
})

export class ListComponent implements OnInit, OnChanges {
  list: {};
  name: '';
  company: string;
  sort: string = 'desc';
  token: boolean = false;
  @Input() listComponentCounter = 0;
  constructor(private httpService: HTTPService, private auth: AuthService) {}

  ngOnInit() {
    this.httpService.getList()
      .subscribe(data => { 
        console.log(data);
        this.list = data;
        this.token = true; 
    });
    this.getCompany();
  }

  ngOnChanges() {
    this.httpService.getList().subscribe(data => { this.list = data });
    console.log(this.list);
  }

  getCompany() {
    const company = this.httpService.getCompany();
    this.company = company
    console.log("this is the ", company);
  }
  
  edit(id, name, surName, mail, phone) {
    const sendId = id;
    const sendName = name.innerHTML.trim();
    const sendSurName = surName.innerHTML.trim();
    const sendMail = mail.innerHTML.trim();
    const sendphone = phone.innerHTML.trim();
    const frontEndData = {sendName, sendSurName, sendMail, sendphone};
    this.httpService.updateUser(sendId, frontEndData)
      .subscribe( data => { this.list = data });
    alert('Employee updated')
  }

  sortByName() {
    console.log(this.sort)
    if(this.sort === 'asc') {
      console.log(this.sort === 'asc')
      this.httpService.sortListDesc().subscribe( data => { this.list = data });
      this.sort = 'desc';
      console.log(this.sort)
    } 
    if (this.sort === 'desc') {
      this.httpService.sortList().subscribe( data => { this.list = data });
      this.sort = 'asc';
    }
  }

  search(term) {
    this.httpService.searchList(term.value).subscribe(data => { this.list = data, console.log(data) });
    term.value = ""
  }

  refresh() {
    this.ngOnInit();
  }

  delete(frontEndId) {
    console.log(frontEndId);
    this.httpService.deleteUser(frontEndId).subscribe(data => { this.ngOnInit() });
  }

  logOut() {
    this.auth.logOutUser();
    this.token = false;
  }
}
