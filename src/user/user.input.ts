import { Component, Input, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { HTTPService } from '../http/http.service';
import { AuthService } from '../app/auth/authService';

@Component({
  selector: 'user-input',
  templateUrl: 'user.input.html',
})

export class UserInput {

  name = "";  
  surName = ""; 
  mail = ""; 
  phone = 0; 
  @Output() emiter = new EventEmitter()

  constructor(private httpService: HTTPService, private authService: AuthService, private router: Router) {}
  
  click(HTMLname, HTMLsurName, HTMLmail, HTMLEndphone) {
    if(!HTMLname || !HTMLsurName || !HTMLmail || !HTMLEndphone) {
      alert('Please enter employee information');
      this.router.navigate(['/list']);
      return
    }
    const userId = this.authService.getId();
    console.log(userId);
    this.httpService.postUser(HTMLname.value, HTMLsurName.value, HTMLmail.value, HTMLEndphone.value, userId)
      .subscribe(responce => {
        console.log(responce)
      })
      this.router.navigate(['/list'])
      
    this.emiter.emit();
  }
}
