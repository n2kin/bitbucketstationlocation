import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { AuthService } from "../app/auth/authService";

@Injectable({ 
  providedIn: 'root'
})

export class HTTPService{

 constructor(private http: HttpClient, private auth: AuthService, private router: Router) {}

 getList() {
    console.log("ova e tokenot: ", this.auth.getToken());
    console.log("ova e ID: ", this.auth.getId())
    const userId = this.auth.getId();
    return this.http.get('http://localhost:3000/', { headers: new HttpHeaders({ 'authorization': this.auth.getToken(), 'user': this.auth.getId() })}, );
    
  }

  getCompany() {
    return this.auth.getCompany();
    // return this.http.get('http://localhost:3000/company');
  }

  sortList() {
    return this.http.get('http://localhost:3000/sort/', { headers: new HttpHeaders({ 'authorization': this.auth.getToken(), 'user' : this.auth.getId() })});
  }

  sortListDesc() {
    return this.http.get('http://localhost:3000/sort/desc/', { headers: new HttpHeaders({ 'authorization': this.auth.getToken(), 'user' : this.auth.getId() })});
  }

  searchList(term) {
    
    const userId = this.auth.getId();
    console.log('Ova e user' ,userId);
    return this.http.get('http://localhost:3000/find/' + term, { headers: new HttpHeaders({ 'authorization': this.auth.getToken(), 'user' : userId })});
  }

  updateUser(sendId, frontEndName) {
    console.log(sendId);
    return this.http.put('http://localhost:3000/update/' + sendId, frontEndName, { headers: new HttpHeaders({ 'authorization': this.auth.getToken() })});
  }

  postUser(HTMLname, HTMLsurName, HTMLmail, HTMLEndphone, userId) {
    console.log(HTMLname, HTMLsurName, HTMLmail, HTMLEndphone)
    const user = { 
      name: HTMLname, 
      surname: HTMLsurName, 
      email: HTMLmail, 
      phone: HTMLEndphone,
      creator: userId,
    }
    return this.http.post('http://localhost:3000/post/user/', user, { headers: new HttpHeaders({ 'authorization': this.auth.getToken() })});
    this.router.navigate(['/list'])
  }
  

  deleteUser(fronEndId) {
    return this.http.delete('http://localhost:3000/delete/' + fronEndId, { headers: new HttpHeaders({ 'authorization': this.auth.getToken() })})
  }
}

