const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const Employee = require('./model/employee');
const User = require('./model/user');
const CheckAuthorisation = require('./middleware/check-authorisation');

const app = express();

app.use(bodyParser.json());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, User');
  next()
});

app.get('/', CheckAuthorisation, (req, res) => {
  Employee.find({ creator: req.headers.user }).then(retrievedData => { res.json(retrievedData) });
});

app.get('/company/', (req, res) => {
  console.log(req.user);
  User.find({ _id: req.user}).then(foundData => { console.log(foundData)});
})

app.get('/sort/', CheckAuthorisation, (req, res) => {
  console.log(req.headers.user);
  Employee.find({ creator: req.headers.user }).sort({ name: 'asc' }).then(sortedData => { res.json(sortedData) });
});

app.get('/sort/desc/', CheckAuthorisation, (req, res) => {
  Employee.find({ creator: req.headers.user }).sort({ name: 'desc' }).then(sortedData => { res.json(sortedData) });
});

app.get('/find/:searchTerm', CheckAuthorisation, (req, res, next) => {
  Employee.find({ creator: req.headers.user, name: req.params.searchTerm }).then(findedData => { if (findedData.length > 0) { return res.json(findedData) }});
  Employee.find({ creator: req.headers.user, surName: req.params.searchTerm }).then(surnameData  => { return res.json(surnameData ) });
})


app.post('/post/user/', CheckAuthorisation, (req, res, next) => {
  const employee = new Employee({
    name: req.body.name,
    surName: req.body.surname,
    mail: req.body.email,
    phone: req.body.phone,
    creator: req.body.creator,
  });
  employee.save()
      .then(result => {
      res.status(201).json({
        message: 'User created',
        result: result,
      })
    })
    .catch(err => {
      res.status(500).json({
        error: err
      })
    })
});

app.post('/api/user/signup', (req, res, next) => {
  const user = new User({
    company: req.body.company,
    email: req.body.email,
    password: req.body.password,
  });
  user.save(function (err) {
    if(err) {
      return res.json({ message: 'There is already user registered with that email adress'})
    } else {
      return res.json({ message: 'User registered'})
    }
  });
});

app.post('/api/user/login', (req, res, next) => {
  User.findOne({ email: req.body.email })
    .then(user => {
      if (!user) {
        return res.json({ message: "Auth failed" });
      }
      if(user.password === req.body.password) {
        const token = jwt.sign({ email: user.email, userId: user._id }, 'Sn#zanao0o09', { expiresIn: '1h' });
        const id = user._id;
        const company = user.company;
        return res.json({ token, id, company, message: "User logged in" });
      } else {
        return res.json({ message: "Wrong password" });
      }
  });
})




app.put('/update/:id', CheckAuthorisation, (req, res, next) => {
  var id = req.params.id;
  Employee.findOne({ _id: id }, function(err, foundObject) {
    foundObject.name = req.body.sendName;
    foundObject.surName = req.body.sendSurName;
    foundObject.mail = req.body.sendMail;
    foundObject.phone = req.body.sendphone;
    foundObject.save();
  });
});

app.delete('/delete/:frontEndId', CheckAuthorisation, (req, res, next) => {
  Employee.deleteOne({ _id: req.params.frontEndId }, function (err) { Employee.find().then(retrievedData => { res.json(retrievedData) }) })
});

app.listen(3000, () => {
  console.log('server started!');
  mongoose.connect('mongodb+srv://Kiril:rngUck0C3yqdTLz0@cluster0-owdfy.mongodb.net/employee-test?retryWrites=true&w=majority', { useNewUrlParser: true , useUnifiedTopology: true })
    .then(() => { console.log('conected to MongoDB') })
});


