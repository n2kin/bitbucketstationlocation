const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
  name: { type: String },
  surName: { type: String },
  mail: { type: String },
  phone: { type: String },
  creator: { type: String }
});

module.exports = mongoose.model('Employee', employeeSchema);